FROM openjdk:8-slim
ADD /build/libs/*-all.jar bot.jar

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENTRYPOINT ["java", "-jar", "/bot.jar"]
