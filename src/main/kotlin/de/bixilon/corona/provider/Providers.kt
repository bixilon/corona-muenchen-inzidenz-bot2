package de.bixilon.corona.provider

import de.bixilon.corona.data.hospitalisierung.HospitalisierungHandler
import de.bixilon.corona.data.hospitalisierung.HospitalisierungProvider
import de.bixilon.corona.data.intensiv.IntensivHandler
import de.bixilon.corona.data.intensiv.IntensivProvider
import de.bixilon.corona.data.inzidenz.InzidenzHandler
import de.bixilon.corona.data.inzidenz.InzidenzProvider
import de.bixilon.corona.data.reproduktionstahl.ReproduktionszahlHandler
import de.bixilon.corona.data.reproduktionstahl.ReproduktionszahlProvider
import de.bixilon.corona.data.signal.SignalHandler
import de.bixilon.corona.data.signal.SignalProvider

object Providers {
    val PROVIDERS: Set<ProviderInformation<*, *>> = setOf(
        ProviderInformation(InzidenzProvider::class, InzidenzHandler),
        ProviderInformation(SignalProvider::class, SignalHandler),
        ProviderInformation(HospitalisierungProvider::class, HospitalisierungHandler),
        ProviderInformation(IntensivProvider::class, IntensivHandler),
        ProviderInformation(ReproduktionszahlProvider::class, ReproduktionszahlHandler),
    )
}
