package de.bixilon.corona.provider

import de.bixilon.corona.data.Handler
import de.bixilon.corona.data.inzidenz.InzidenzProvider
import kotlin.reflect.KClass
import kotlin.reflect.KFunction1

data class ProviderInformation<T : Any, V:Any>(
    val providerClass: KClass<T>,
    val handler: Handler<T, V>,
)
