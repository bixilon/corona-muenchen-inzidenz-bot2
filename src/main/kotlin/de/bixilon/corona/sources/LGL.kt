package de.bixilon.corona.sources

import de.bixilon.corona.data.hospitalisierung.Hospitalisierung
import de.bixilon.corona.data.hospitalisierung.HospitalisierungProvider
import de.bixilon.corona.data.intensiv.Intensiv
import de.bixilon.corona.data.intensiv.IntensivProvider
import de.bixilon.corona.data.inzidenz.Inzidenz
import de.bixilon.corona.data.inzidenz.InzidenzProvider
import de.bixilon.corona.data.reproduktionstahl.Reproduktionszahl
import de.bixilon.corona.data.reproduktionstahl.ReproduktionszahlProvider
import org.joda.time.LocalDate
import org.jsoup.Connection
import org.jsoup.Jsoup
import java.text.SimpleDateFormat

object LGL : Source, InzidenzProvider, HospitalisierungProvider, IntensivProvider, ReproduktionszahlProvider {
    private val INZIDENZ_REGEX = "münchen&nbsp;(\\()?stadt(\\))?<\\/td>(<td>[a-z+\\-.,\\d)(]*<\\/td>*){4}<td>([\\d.]+(,\\d+)?)<\\/td>".toRegex()
    private val HOSPITALISIERUNG_REGEX = "<h4><strong>gemeldetehospitalisiertefällederletzten7tage<\\/strong><\\/h4>[\\/<>\\w\\d\"=.äöü:)(\\-,]*<pclass=\"card-text\"><strong>(\\d*\\.?\\d+)".toRegex()
    private val INTENSIV_REGEX = "<h4>intensivbetten\\(divi\\)<\\/h4><pclass=\"card-text\"><strong>(\\d*\\.?\\d+)&nbsp".toRegex()
    private val INTENSIV_DATE_REGEX = "intensivregister<\\/a>&nbsp;&mdash;&nbsp;stand:(\\d{1,2}\\.\\d{1,2}\\.\\d{2,4}),\\d{1,2}:\\d{1,2}uhr".toRegex()
    private val REPRODUKTIONSZAHL_REGEX = "<\\/div><dlclass=\"horizontal_zwei\"><dt>r\\(t\\)-wert<sup>1\\)<\\/sup><\\/dt><dd>(\\d,\\d*)<\\/dd>".toRegex()
    override val name: String = "LGL"
    override val url: String = "https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/"
    private val DATE_FORMAT = SimpleDateFormat("dd.MM.yyyy")
    private val DATE_REGEX = "&nbsp;&mdash;&nbsp;stand:(\\d{1,2}\\.\\d{1,2}\\.\\d{2,4}),\\d{1,2}:\\d{1,2}uhr".toRegex()

    override lateinit var inzidenz: Inzidenz
    override lateinit var hospitalisierung: Hospitalisierung
    override lateinit var intensiv: Intensiv
    override lateinit var reproduktionszahl: Reproduktionszahl

    private lateinit var html: String
    private lateinit var date: LocalDate


    override fun prepareFetch() {
        val response = Jsoup.connect(url)
            .method(Connection.Method.GET)
            .ignoreContentType(true)
            .followRedirects(true)
            .execute()

        html = response.body().replace("\n", "").replace("\r", "").replace(" ", "").lowercase()


        date = LocalDate(DATE_FORMAT.parse(DATE_REGEX.find(html)!!.groups[1]!!.value))
    }

    override fun fetchInzidenz() {
        val groups = INZIDENZ_REGEX.find(html)!!.groups
        val inzidenz = groups[4]!!.value.replace(".", "").replace(',', '.').toFloat()

        this.inzidenz = Inzidenz(date, inzidenz)
    }

    override fun fetchHospitalisierung() {
        val groups = HOSPITALISIERUNG_REGEX.find(html)!!.groups

        this.hospitalisierung = Hospitalisierung(date, cases = groups[1]!!.value.replace(".", "").toInt())
    }

    override fun fetchIntensiv() {
        val groups = INTENSIV_REGEX.find(html)!!.groups

        val date = LocalDate(DATE_FORMAT.parse(INTENSIV_DATE_REGEX.find(html)!!.groups[1]!!.value)).plusDays(1) // The LGL displays the date of the numbers, not the last update
        this.intensiv = Intensiv(date, cases = groups[1]!!.value.replace(".", "").toInt())
    }

    override fun fetchReproduktionszahl() {
        val groups = REPRODUKTIONSZAHL_REGEX.find(html)!!.groups
        val reproduktionszahl = groups[1]!!.value.replace(',', '.').toFloat()

        this.reproduktionszahl = Reproduktionszahl(date, reproduktionszahl)
    }
}
