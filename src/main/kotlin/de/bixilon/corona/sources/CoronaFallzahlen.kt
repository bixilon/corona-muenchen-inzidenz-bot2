package de.bixilon.corona.sources

import de.bixilon.corona.data.inzidenz.Inzidenz
import de.bixilon.corona.data.inzidenz.InzidenzProvider
import de.bixilon.corona.data.reproduktionstahl.Reproduktionszahl
import de.bixilon.corona.data.reproduktionstahl.ReproduktionszahlProvider
import org.joda.time.LocalDate
import org.jsoup.Connection
import org.jsoup.Jsoup
import java.text.SimpleDateFormat
import java.util.*

object CoronaFallzahlen : Source, InzidenzProvider, ReproduktionszahlProvider {
    private val FILTER_REGEX = "<(\\s)*(/)?(\\s)*strong(\\s)*>".toRegex()
    private val INZIDENZ_REGEX = "7-tage-inzidenz für münchen beträgt laut rki ((\\d*\\.)?\\d{1,3}([,.]\\d)?)".toRegex()
    private val REPRODUKTIONSZAHL_REGEX = "die reproduktionszahl für münchen liegt bei (\\d,\\d*) \\(stand (\\d+\\.\\d+\\.)".toRegex()
    private val DATE_REGEX = "update (\\d+\\.\\d+)\\.:".toRegex()
    private val DATE_FORMAT = SimpleDateFormat("dd.MM")

    override val name: String = "Corona-Fallzahlen"
    override val url: String = "https://stadt.muenchen.de/infos/corona-fallzahlen-muenchen"

    override lateinit var inzidenz: Inzidenz
    override lateinit var reproduktionszahl: Reproduktionszahl

    private lateinit var html: String
    private lateinit var date: LocalDate

    override fun prepareFetch() {
        val response = Jsoup.connect(url)
            .method(Connection.Method.GET)
            .followRedirects(false)
            .execute()

        html = response.body().replace(FILTER_REGEX, "").replace("  ", " ").replace("\n", "").replace("\r", "").replace("<b>", "").replace("</b>", "").lowercase()

        var highestDate: LocalDate? = null
        for (match in DATE_REGEX.findAll(html)) {
            val date = LocalDate(DATE_FORMAT.parse(match.groups[1]!!.value).apply { year = Date().year })
            if (highestDate == null || date > highestDate) {
                highestDate = date
            }
        }

        this.date = highestDate!!
    }

    override fun fetchInzidenz() {
        val groups = INZIDENZ_REGEX.find(html)!!.groups
        val inzidenz = groups[1]!!.value.replace(".", "").replace(',', '.').toFloat()

        this.inzidenz = Inzidenz(date, inzidenz)
    }

    override fun fetchReproduktionszahl() {
        val groups = REPRODUKTIONSZAHL_REGEX.find(html)!!.groups
        val value = groups[1]!!.value.replace(',', '.').toFloat()
        val date = LocalDate(DATE_FORMAT.parse(groups[2]!!.value).apply { year = Date().year })

        this.reproduktionszahl = Reproduktionszahl(date, value)
    }
}
