package de.bixilon.corona.sources

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import de.bixilon.corona.JSON
import de.bixilon.corona.data.inzidenz.Inzidenz
import de.bixilon.corona.data.inzidenz.InzidenzProvider
import org.joda.time.LocalDate
import org.jsoup.Connection
import org.jsoup.Jsoup
import java.io.StringReader
import java.text.SimpleDateFormat

object RKI : Source, InzidenzProvider {
    private const val API_URL = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/RKI_Landkreisdaten/FeatureServer/0/query?where=county%3D%27SK%20M%C3%BCnchen%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=none&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=GEN%2Ccounty%2Ccases7_per_100k_txt%2Clast_update&returnGeometry=false&returnCentroid=false&featureEncoding=esriDefault&multipatchOption=xyFootprint&maxAllowableOffset=&geometryPrecision=&outSR=&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=false&quantizationParameters=&sqlFormat=none&f=pjson&token="
    override val name: String = "RKI"
    override val url: String = "https://corona.rki.de/"
    private val DATE_FORMAT = SimpleDateFormat("dd.MM.yyyy', 00:00 Uhr'")

    override lateinit var inzidenz: Inzidenz

    private lateinit var lastData: JsonObject

    override fun fetchInzidenz() {
        for (place in (lastData["features"] as JsonArray<JsonObject>)) {
            val attribute = place["attributes"] as JsonObject
            if (attribute["county"] as String != "SK München") {
                continue
            }
            val date = LocalDate(DATE_FORMAT.parse(attribute["last_update"] as String))
            val value = (attribute["cases7_per_100k_txt"] as String).replace(',', '.').toFloat()
            inzidenz = Inzidenz(date, value)
            return
        }
        error("Could not find SK München!")
    }


    override fun prepareFetch() {
        val response = Jsoup.connect(API_URL)
            .method(Connection.Method.GET)
            .ignoreContentType(true)
            .followRedirects(false)
            .execute()

        lastData = JSON.KLAXON.parseJsonObject(StringReader(response.body()))
    }
}
