package de.bixilon.corona.sources

interface Source {
    val name: String
    val url: String

    fun prepareFetch()
}
