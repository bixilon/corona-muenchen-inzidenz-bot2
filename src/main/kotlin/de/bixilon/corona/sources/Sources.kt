package de.bixilon.corona.sources

object Sources {
    val SOURCES: List<Source> = listOf(
        RKI,
        LGL,
        CoronaFallzahlen,
    )
}
