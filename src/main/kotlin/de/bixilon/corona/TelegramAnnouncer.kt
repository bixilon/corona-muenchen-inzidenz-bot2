package de.bixilon.corona

import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.ParseMode
import de.bixilon.corona.config.Config

object TelegramAnnouncer {
    private var toAnnounce = ""
    private val BOT = bot {
        token = Config.telegramAPIToken
        dispatch {
            println("Telegram bot dispatched!")
        }
        // logLevel = LogLevel.All()
    }

    @Synchronized
    fun announce(message: String) {
        if (toAnnounce.isNotEmpty()) {
            toAnnounce += "\n\n"
        }
        toAnnounce += message
    }

    @Synchronized
    fun sendBatch() {
        if (toAnnounce.isBlank()) {
            return
        }
        BOT.sendMessage(
            chatId = ChatId.fromId(Config.channelId),
            disableWebPagePreview = true,
            parseMode = ParseMode.MARKDOWN,
            text = toAnnounce,
        )
        println("[Telegram] $toAnnounce")
        toAnnounce = ""
    }

    fun error(message: String) {
        if (Config.errorChannelId == 0L) {
            return
        }
        BOT.sendMessage(
            chatId = ChatId.fromId(Config.errorChannelId),
            disableWebPagePreview = true,
            parseMode = ParseMode.MARKDOWN,
            text = message,
        )
        println("[Telegram error] $message")
    }
}
