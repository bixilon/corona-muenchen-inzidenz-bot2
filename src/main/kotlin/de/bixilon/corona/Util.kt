package de.bixilon.corona

import java.io.PrintWriter
import java.io.StringWriter
import java.util.*

object Util {
    fun Throwable.toStackTrace(): String {
        val stringWriter = StringWriter()
        this.printStackTrace(PrintWriter(stringWriter))
        return stringWriter.toString()
    }

    fun now(): Date {
        return Date(System.currentTimeMillis())
    }

    fun Float.format(): String {
        return "$this".replace('.', ',')
    }
}
