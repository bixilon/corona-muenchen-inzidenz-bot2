package de.bixilon.corona.config

import com.beust.klaxon.Json
import com.beust.klaxon.JsonObject
import de.bixilon.corona.JSON
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.StringReader
import java.lang.reflect.Modifier
import java.nio.charset.StandardCharsets
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.jvm.javaField
import kotlin.system.exitProcess

object Config {
    @Transient
    private const val CONFIG_PATH = "config/config.json"

    @Json(name = "poll_delay") var pollDelay: Int = 180
    @Json(name = "telegram_api_token") var telegramAPIToken: String = ""
    @Json(name = "channel_id") var channelId: Long = 0
    @Json(name = "error_channel_id") var errorChannelId: Long = 0

    @Json(name = "previous_values") var previousValues: MutableMap<String, JsonObject> = mutableMapOf()

    @Json(name = "previous_version") var previousVersion = "60aa511fe89871848cae9fbc8c65f765afa25158" // static here, this system was implemented and pushed after that commit


    @Synchronized
    fun read() {
        val file = File(CONFIG_PATH)
        if (!file.exists()) {
            save()
            println("Config generated! Please enter the details to continue")
            exitProcess(1)
        }

        val input = FileInputStream(file)

        val jsonObject = JSON.KLAXON.parseJsonObject(StringReader(String(input.readBytes())))


        for (field in Config::class.declaredMemberProperties) {
            val javaField = field.javaField ?: continue
            if (field.isConst) {
                continue
            }
            if (Modifier.isTransient(javaField.modifiers)) {
                continue
            }
            javaField.isAccessible = true

            val name = when {
                field.hasAnnotation<Json>() -> field.findAnnotation<Json>()!!.name
                else -> field.name
            }
            jsonObject[name]?.let {
                javaField.set(this, it)
            }
        }

        check(telegramAPIToken.isNotBlank() && telegramAPIToken.length > 20) { "Your Telegram bot token seems to be invalid! Please check again!" }
        check(channelId != 0L) { "Your channel id seems to be invalid! Please check again!" }
        save()
    }

    @Synchronized
    fun save() {
        val file = File(CONFIG_PATH)
        file.parentFile.mkdirs()
        val json = JSON.KLAXON.toJsonString(this)

        val outputStream = FileOutputStream(CONFIG_PATH)

        outputStream.write(json.toByteArray(StandardCharsets.UTF_8))
        outputStream.close()
    }
}
