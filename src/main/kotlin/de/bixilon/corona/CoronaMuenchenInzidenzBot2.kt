package de.bixilon.corona

import de.bixilon.corona.config.Config
import de.bixilon.corona.data.GeneralHandler
import de.bixilon.corona.data.Handler
import de.bixilon.corona.provider.Providers
import de.bixilon.corona.sources.Source
import de.bixilon.corona.sources.Sources
import de.bixilon.corona.updater.UpdateChecker

object CoronaMuenchenInzidenzBot2 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Starting...")
        Config.read()

        UpdateChecker.checkAndAnnounceUpdate()

        println("Polling data from ${Sources.SOURCES.size} source(s), with a delay of ${Config.pollDelay} seconds!")
        while (true) {
            work()
            Thread.sleep(Config.pollDelay * 1000L)
        }
    }

    private fun work() {
        for (source in Sources.SOURCES) {
            try {
                source.prepareFetch()
            } catch (error: Throwable) {
                error.handle(source)
                continue
            }
            for ((`class`, handler) in Providers.PROVIDERS) {
                if (!`class`.java.isAssignableFrom(source::class.java)) {
                    continue
                }
                try {
                    handler.fetch(source)
                    handler.handle(source)
                } catch (error: Throwable) {
                    error.handle(source, handler)
                }
            }
        }
        TelegramAnnouncer.sendBatch()
    }

    private fun Throwable.handle(source: Source, handler: Handler<*, *> = GeneralHandler) {
        this.printStackTrace()
        try {
            handler.onError(source, this)
        } catch (error: Throwable) {
            println("Could not handle error!")
            error.printStackTrace()
        }
    }
}
