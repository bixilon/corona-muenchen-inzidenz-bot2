package de.bixilon.corona.data

import de.bixilon.corona.sources.Source


object GeneralHandler: Handler<Any, Any>() {

    override fun fetch(source: Source) {
        error("Can not fetch general source!")
    }

    override fun handle(source: Source) {
        error("Can not handle general source!")
    }
}
