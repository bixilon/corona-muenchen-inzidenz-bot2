package de.bixilon.corona.data.inzidenz

interface InzidenzProvider {
    val inzidenz: Inzidenz


    fun fetchInzidenz()
}
