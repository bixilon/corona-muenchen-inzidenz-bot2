package de.bixilon.corona.data.inzidenz

import com.beust.klaxon.JsonObject
import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.Util.format
import de.bixilon.corona.config.Config
import de.bixilon.corona.data.Handler
import de.bixilon.corona.sources.Source
import org.joda.time.LocalDate

object InzidenzHandler : Handler<InzidenzProvider, Inzidenz>() {
    private var previousInzidenz: Inzidenz? = Config.previousValues[InzidenzHandler::class.java.simpleName]?.let {
        return@let Inzidenz(
            date = LocalDate.parse(it["date"].toString()),
            value = (it["value"] as Number).toFloat(),
        )
    }
        set(value) {
            field = value
            value?.let {
                Config.previousValues[InzidenzHandler::class.java.simpleName] = JsonObject(mutableMapOf(
                    "date" to it.date.toString(),
                    "value" to it.value,
                ))
                Config.save()
            }
        }


    override fun handle(source: Source) {
        if (isErrored(source)) {
            return
        }
        check(source is InzidenzProvider)
        val inzidenz = source.inzidenz

        clearError(source)

        previousInzidenz?.let {
            // Check if value is the same or already announced (today)
            if (inzidenz.date <= it.date) {
                return
            }
            if (inzidenz.value == it.value) {
                return
            }
        }

        TelegramAnnouncer.announce("7-Tage-Inzidenz: `${inzidenz.value.format()}`. (Quelle: [${source.name}](${source.url}))")
        previousInzidenz = inzidenz
    }


    override fun fetch(source: Source) {
        check(source is InzidenzProvider)

        if (isErrored(source)) {
            return
        }

        source.fetchInzidenz()
    }
}
