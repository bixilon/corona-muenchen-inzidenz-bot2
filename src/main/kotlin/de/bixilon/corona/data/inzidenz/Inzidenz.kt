package de.bixilon.corona.data.inzidenz

import de.bixilon.corona.data.CollectedData
import org.joda.time.LocalDate

data class Inzidenz(
    override val date: LocalDate,
    val value: Float,
): CollectedData
