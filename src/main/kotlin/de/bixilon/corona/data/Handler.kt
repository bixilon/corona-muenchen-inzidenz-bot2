package de.bixilon.corona.data

import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.Util.now
import de.bixilon.corona.Util.toStackTrace
import de.bixilon.corona.sources.Source
import java.util.*


abstract class Handler<T, V> {
    private var errorCounter: MutableMap<Source, Int> = mutableMapOf()
    private var lastError: MutableMap<Source, Date> = mutableMapOf()

    abstract fun fetch(source: Source)

    abstract fun handle(source: Source)

    open fun onError(source: Source, error: Throwable) {
        var errorCount = 0
        errorCounter[source]?.let { errorCount = it }
        if (error is NotImplementedError) {
            errorCount = MAX_ERRORS
        } else {
            errorCount++
        }
        errorCounter[source] = errorCount
        lastError[source] = now()

        if (errorCount != MAX_ERRORS) {
            // Do not resend every time
            return
        }


        TelegramAnnouncer.error("""
Corona Inzidenz München Bot2
Error in source: ${source.name}:

```
${error.toStackTrace()}
```
        """)
    }

    protected fun clearError(source: Source) {
        errorCounter.remove(source)
        lastError.remove(source)
    }

    protected fun isErrored(source: Source): Boolean {
        if ((errorCounter[source] ?: 0) > MAX_ERRORS) {
            lastError[source]?.let {
                if (it >= Date(System.currentTimeMillis() + IGNORE_ERROR_TIME)) {
                    lastError.remove(source)
                    return@let
                }
                return true
            }
        }
        return false
    }

    private companion object {
        private const val IGNORE_ERROR_TIME = 1000L * 60L * 60L * 10L // 1000ms * 60s * 60m * 10h
        private const val MAX_ERRORS = 5
    }
}
