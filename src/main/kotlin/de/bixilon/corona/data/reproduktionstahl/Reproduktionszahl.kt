package de.bixilon.corona.data.reproduktionstahl

import de.bixilon.corona.data.CollectedData
import org.joda.time.LocalDate

data class Reproduktionszahl(
    override val date: LocalDate,
    val value: Float,
): CollectedData
