package de.bixilon.corona.data.reproduktionstahl

interface ReproduktionszahlProvider {
    val reproduktionszahl: Reproduktionszahl

    fun fetchReproduktionszahl()
}
