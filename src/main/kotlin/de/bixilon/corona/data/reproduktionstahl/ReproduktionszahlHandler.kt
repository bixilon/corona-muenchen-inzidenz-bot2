package de.bixilon.corona.data.reproduktionstahl

import com.beust.klaxon.JsonObject
import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.Util.format
import de.bixilon.corona.config.Config
import de.bixilon.corona.data.Handler
import de.bixilon.corona.sources.Source
import org.joda.time.LocalDate

object ReproduktionszahlHandler : Handler<ReproduktionszahlProvider, Reproduktionszahl>() {
    private var previousReproduktionszahl: Reproduktionszahl? = Config.previousValues[ReproduktionszahlHandler::class.java.simpleName]?.let {
        return@let Reproduktionszahl(
            date = LocalDate.parse(it["date"].toString()),
            value = (it["value"] as Number).toFloat(),
        )
    }
        set(value) {
            field = value
            value?.let {
                Config.previousValues[ReproduktionszahlHandler::class.java.simpleName] = JsonObject(mutableMapOf(
                    "date" to it.date.toString(),
                    "value" to it.value,
                ))
                Config.save()
            }
        }


    override fun handle(source: Source) {
        if (isErrored(source)) {
            return
        }
        check(source is ReproduktionszahlProvider)
        val reproduktionszahl = source.reproduktionszahl

        clearError(source)

        previousReproduktionszahl?.let {
            // Check if value is the same or already announced (today)
            if (reproduktionszahl.date <= it.date) {
                return
            }
            if (reproduktionszahl.value == it.value) {
                return
            }
        }

        TelegramAnnouncer.announce("Reproduktionszahl: `${reproduktionszahl.value.format()}`. (Quelle: [${source.name}](${source.url}))")
        previousReproduktionszahl = reproduktionszahl
    }


    override fun fetch(source: Source) {
        check(source is ReproduktionszahlProvider)

        if (isErrored(source)) {
            return
        }

        source.fetchReproduktionszahl()
    }
}
