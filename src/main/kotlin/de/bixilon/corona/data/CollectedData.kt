package de.bixilon.corona.data

import org.joda.time.LocalDate

interface CollectedData {
    val date: LocalDate
}
