package de.bixilon.corona.data.intensiv

import com.beust.klaxon.JsonObject
import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.config.Config
import de.bixilon.corona.data.Handler
import de.bixilon.corona.sources.Source
import org.joda.time.LocalDate

object IntensivHandler : Handler<IntensivProvider, Intensiv>() {
    private var previousIntensiv: Intensiv? = Config.previousValues[IntensivHandler::class.java.simpleName]?.let {
        return@let Intensiv(
            date = LocalDate.parse(it["date"].toString()),
            cases = (it["cases"] as Number).toInt(),
        )
    }
        set(value) {
            field = value
            value?.let {
                Config.previousValues[IntensivHandler::class.java.simpleName] = JsonObject(mutableMapOf(
                    "date" to it.date.toString(),
                    "cases" to it.cases,
                ))
                Config.save()
            }
        }


    override fun handle(source: Source) {
        if (isErrored(source)) {
            return
        }
        check(source is IntensivProvider)
        val intensiv = source.intensiv

        clearError(source)

        previousIntensiv?.let {
            // Check if value is the same or already announced (today)
            if (intensiv.date <= it.date) {
                return
            }
            if (intensiv.cases == it.cases) {
                return
            }
        }

        TelegramAnnouncer.announce("Patienten Intensivstation (7 Tage): `${intensiv.cases}`. (Quelle: [${source.name}](${source.url}))")
        previousIntensiv = intensiv
    }


    override fun fetch(source: Source) {
        check(source is IntensivProvider)

        if (isErrored(source)) {
            return
        }

        source.fetchIntensiv()
    }
}
