package de.bixilon.corona.data.intensiv

import de.bixilon.corona.data.CollectedData
import org.joda.time.LocalDate

data class Intensiv(
    override val date: LocalDate,
    val cases: Int,
) : CollectedData
