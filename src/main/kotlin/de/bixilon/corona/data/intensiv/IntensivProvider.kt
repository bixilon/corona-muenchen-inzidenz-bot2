package de.bixilon.corona.data.intensiv

interface IntensivProvider {
    val intensiv:Intensiv

    fun fetchIntensiv()
}
