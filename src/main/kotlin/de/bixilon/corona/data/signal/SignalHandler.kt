package de.bixilon.corona.data.signal

import com.beust.klaxon.JsonObject
import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.config.Config
import de.bixilon.corona.data.Handler
import de.bixilon.corona.sources.Source
import org.joda.time.LocalDate

object SignalHandler : Handler<SignalProvider, Signal>() {
    private var previousSignal: Signal? = Config.previousValues[SignalHandler::class.java.simpleName]?.let {
        return@let Signal(
            date = LocalDate.parse(it["date"].toString()),
            value = Signals.valueOf(it["value"] as String),
        )
    }
        set(value) {
            field = value
            value?.let {
                Config.previousValues[SignalHandler::class.java.simpleName] = JsonObject(mutableMapOf(
                    "date" to it.date.toString(),
                    "value" to it.value,
                ))
                Config.save()
            }
        }


    override fun handle(source: Source) {
        if (isErrored(source)) {
            return
        }
        check(source is SignalProvider)
        val signal = source.signal

        clearError(source)

        previousSignal?.let {
            // Check if value is the same or already announced (today)
            if (signal.date <= it.date) {
                return
            }
            if (signal.value == it.value) {
                return
            }
        }
        val name = when (signal.value) {
            Signals.UNDER_35 -> "Unter 35"
            Signals.ABOVE_35 -> "Über 35"
            Signals.LEVEL_YELLOW -> "Gelb"
            Signals.LEVEL_RED -> "Rot"
        }

        TelegramAnnouncer.announce("Einstufung: \"$name\". (Quelle: [${source.name}](${source.url}))")
        previousSignal = signal
    }


    override fun fetch(source: Source) {
        check(source is SignalProvider)

        if (isErrored(source)) {
            return
        }

        source.fetchSignal()
    }
}
