package de.bixilon.corona.data.signal

enum class Signals {
    UNDER_35,
    ABOVE_35,
    LEVEL_YELLOW,
    LEVEL_RED,
}
