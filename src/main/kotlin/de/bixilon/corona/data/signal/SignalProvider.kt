package de.bixilon.corona.data.signal

import de.bixilon.corona.data.signal.Signal

interface SignalProvider {
    val signal: Signal


    fun fetchSignal()
}
