package de.bixilon.corona.data.signal

import de.bixilon.corona.data.CollectedData
import org.joda.time.LocalDate

data class Signal(
    override val date: LocalDate,
    val value: Signals,
): CollectedData
