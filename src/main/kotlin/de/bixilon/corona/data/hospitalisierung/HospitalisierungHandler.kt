package de.bixilon.corona.data.hospitalisierung

import com.beust.klaxon.JsonObject
import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.config.Config
import de.bixilon.corona.data.Handler
import de.bixilon.corona.sources.Source
import org.joda.time.LocalDate

object HospitalisierungHandler : Handler<HospitalisierungProvider, Hospitalisierung>() {
    private var previousHospitalisering: Hospitalisierung? = Config.previousValues[HospitalisierungHandler::class.java.simpleName]?.let {
        return@let Hospitalisierung(
            date = LocalDate.parse(it["date"].toString()),
            cases = (it["cases"] as Number).toInt(),
        )
    }
        set(value) {
            field = value
            value?.let {
                Config.previousValues[HospitalisierungHandler::class.java.simpleName] = JsonObject(mutableMapOf(
                    "date" to it.date.toString(),
                    "cases" to it.cases,
                ))
                Config.save()
            }
        }


    override fun handle(source: Source) {
        if (isErrored(source)) {
            return
        }
        check(source is HospitalisierungProvider)
        val hospitalisierung = source.hospitalisierung

        clearError(source)

        previousHospitalisering?.let {
            // Check if value is the same or already announced (today)
            if (hospitalisierung.date <= it.date) {
                return
            }
            if (hospitalisierung.cases == it.cases) {
                return
            }
        }

        TelegramAnnouncer.announce("Hospitalisierte Fälle (7 Tage): `${hospitalisierung.cases}`. (Quelle: [${source.name}](${source.url}))")
        previousHospitalisering = hospitalisierung
    }


    override fun fetch(source: Source) {
        check(source is HospitalisierungProvider)

        if (isErrored(source)) {
            return
        }

        source.fetchHospitalisierung()
    }
}
