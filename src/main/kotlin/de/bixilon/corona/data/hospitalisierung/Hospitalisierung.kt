package de.bixilon.corona.data.hospitalisierung

import de.bixilon.corona.data.CollectedData
import org.joda.time.LocalDate

data class Hospitalisierung(
    override val date: LocalDate,
    val cases: Int,
) : CollectedData
