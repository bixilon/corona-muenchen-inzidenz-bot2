package de.bixilon.corona.data.hospitalisierung

interface HospitalisierungProvider {
    val hospitalisierung: Hospitalisierung

    fun fetchHospitalisierung()
}
