package de.bixilon.corona.updater

import de.bixilon.corona.JSON
import de.bixilon.corona.TelegramAnnouncer
import de.bixilon.corona.config.Config
import java.io.StringReader

object UpdateChecker {

    fun checkAndAnnounceUpdate() {
        try {
            val oldVersion = Config.previousVersion
            if (oldVersion.isBlank()) {
                return
            }
            val versions = JSON.KLAXON.parseArray<Commit>(StringReader(String(UpdateChecker::class.java.getResourceAsStream("/git.json")!!.readBytes())))!!
            val newest = versions.first()
            if (newest.commit == oldVersion) {
                return
            }
            var announcement = """Der Bot wurde von Version `${oldVersion.substring(0, 7)}` auf `${newest.commit.substring(0, 7)}` geupdated:""".trimMargin()

            for (version in versions) {
                if (oldVersion == version.commit) {
                    break
                }
                announcement += "\n - `${version.commit.substring(0, 7)}`: ${version.message}"
            }

            Config.previousVersion = newest.commit
            Config.save()

            TelegramAnnouncer.announce(announcement)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }

    }

    data class Commit(
        val commit: String,
        val author: String,
        val date: String,
        val message: String,
    )
}
