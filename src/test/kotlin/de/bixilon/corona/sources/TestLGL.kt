package de.bixilon.corona.sources

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestLGL {

    @Test
    @BeforeAll
    fun prepare() {
        LGL.prepareFetch()
    }

    @Test
    fun fetchInzidenz() {
        LGL.fetchInzidenz()
    }

    @Test
    fun fetchHospitalisierung() {
        LGL.fetchHospitalisierung()
    }

    @Test
    fun fetchIntensiv() {
        LGL.fetchIntensiv()
    }

    @Test
    fun fetchReproduktionszahl() {
        LGL.fetchReproduktionszahl()
    }
}
