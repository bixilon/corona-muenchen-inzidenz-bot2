package de.bixilon.corona.sources

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestRKI {

    @Test
    @BeforeAll
    fun prepare() {
        RKI.prepareFetch()
    }

    @Test
    fun fetchInzidenz() {
        RKI.fetchInzidenz()
    }
}
