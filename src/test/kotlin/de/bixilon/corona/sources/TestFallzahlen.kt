package de.bixilon.corona.sources

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestFallzahlen {

    @Test
    @BeforeAll
    fun prepare() {
        CoronaFallzahlen.prepareFetch()
    }

    @Test
    fun fetchInzidenz() {
        CoronaFallzahlen.fetchInzidenz()
    }

    @Test
    fun fetchReproduktionszahl() {
        CoronaFallzahlen.fetchReproduktionszahl()
    }
}
