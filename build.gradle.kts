import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.0.0"
    application
}

group = "de.bixilon"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("org.jsoup:jsoup:1.14.3")
    implementation("com.beust:klaxon:5.5")
    implementation("io.github.kotlin-telegram-bot.kotlin-telegram-bot:telegram:6.0.5")
    implementation(kotlin("reflect"))
    implementation("joda-time:joda-time:2.10.13")

}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("de.bixilon.corona.CoronaMuenchenInzidenzBot2")
}

tasks {
    processResources {
        doLast {
            val file = File("$buildDir/resources/main", "git.json")
            file.parentFile.mkdirs()
            // thanks https://gist.github.com/textarcana/1306223
            file.writeText("""git log --pretty=format:'{%n  "commit": "%H",%n  "author": "%aN <%aE>",%n  "date": "%ad",%n  "message": "%s"%n},' ${'$'}@ | perl -pe 'BEGIN{print "["}; END{print "]\n"}' | perl -pe 's/},]/}]/'""".runCommand())
            print("Generated git info")
        }
    }
    shadowJar {
        manifest {
            attributes("Main-Class" to "de.bixilon.corona.CoronaMuenchenInzidenzBot2")
        }
    }
}

fun String.runCommand(
    workingDir: File = File(projectDir.path),
    timeoutAmount: Long = 60,
    timeoutUnit: TimeUnit = TimeUnit.SECONDS,
): String = ProcessBuilder("bash", "-c", this)
    .directory(workingDir)
    .redirectOutput(ProcessBuilder.Redirect.PIPE)
    .redirectError(ProcessBuilder.Redirect.PIPE)
    .start()
    .apply { waitFor(timeoutAmount, timeoutUnit) }
    .run {
        inputStream.bufferedReader().readText().trim()
    }
